<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Short Link</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
</head>
<body>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Create Short Link</div>
                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if(session('url'))
                        <div class="alert alert-success" role="alert">
                            {{ session('url') }}
                        </div>
                    @endif
                    <form action="{{ route('url.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="original_url">URL</label>
                            <input type="url" name="original_url" id="original_url" value="{{ old('original_url') }}"
                                   class="form-control @error('original_url') is-invalid @enderror"
                                   required>
                            @error('original_url')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="max_transitions">Max Transitions (0 = Unlimited)</label>
                            <input type="number" name="max_transitions" value="{{ old('max_transitions') ?? 0 }}"
                                   id="max_transitions" class="form-control" required>
                            @error('max_transitions')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="expiry">Expiry (Not more than 24 hours)</label>
                            <input type="text" id="expiry" placeholder="Time" name="expiry"
                                   value="{{ old('expiry') ?? '00:00' }}"
                                   class="form-control">
                            @error('expiry')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Generate Short Link</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#expiry').timepicker({
        format: 'HH:MM',
        mode: '24hr'
    });
</script>
</body>
</html>
