<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Url extends Model
{
    use HasFactory;
    protected $guarded = false;

    public function checkTransitionsUsed(): bool
    {
        return $this->max_transitions != 0 && $this->transitions >= $this->max_transitions;
    }

    public function checkTimePassed(): bool
    {
        $dateTime1 = Carbon::parse($this->created_at);
        $time2 = Carbon::parse($this->expiry);

        $dateTime1->addHours($time2->hour)->addMinutes($time2->minute);

        return Carbon::now()->diffInSeconds($dateTime1) <= 0;
    }
}
