<?php

namespace App\Http\Requests\Url;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'original_url' => 'required|url',
            'max_transitions' => 'required|integer|min:0',
            'expiry' => 'required|date_format:H:i|before:24:00|after:00:00',
        ];
    }
}
