<?php

namespace App\Http\Controllers;

use App\Http\Requests\Url\StoreRequest;
use App\Models\Url;
use Illuminate\Support\Str;

class UrlController extends Controller
{
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('create_form');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $url = Url::create($request->validated() + [
            'shortener_url' => Str::random(8),
        ]);

        return redirect()->route('url.create')->with('success', 'Short link generated successfully!')->with('url', route('url.redirect', $url));
    }

    /**
     * Display the specified resource.
     */
    public function redirectToOriginal(Url $url)
    {
        if($url->checkTransitionsUsed() || $url->checkTimePassed()){
            abort(404);
        }

        $url->increment('transitions');

        return redirect($url->original_url);
    }

}
