<?php

use App\Http\Controllers\UrlController;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'url.'], function (){
    Route::get('/', [UrlController::class, 'create'])->name('create');
    Route::post('/store', [UrlController::class, 'store'])->name('store');
    Route::get('/{url:shortener_url}', [UrlController::class, 'redirectToOriginal'])->name('redirect');
});
